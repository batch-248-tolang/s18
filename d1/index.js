// console.log("Hello world")

// Functions
	// Functions are mostly created for complicated task to run several lines of code in succession.
	// Functions are also used to prevent repeating line/block of codes that perform the same task/functionality.

	let nickName = "Chris";

	function printInput() {
		// let nickName = prompt("Enter your nickname:");
		console.log("Hi, " + nickName)
	}

	// printInput()

	// Consider this function
		// parameter - (name)
	function printName(name) {
		console.log("My nickname is " + name);
	}

	// argument
	printName("Chris");
	printName("John");

	// You can directly pass data into the function.

	// [SECTION] Parameters and Arguments

	// Parameter
		// "name" is called a parameter.
		// a "parameter" acts as a named variable that only exist inside a function.

	// Arguments
		// "Chris" the information provided directly into the function is called an argument
		// Values passed when invoking the function. Ex. functionName(arguments);

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(25);

	/*
		Mini activity
		Create a function than can check the divisibility by 4
		1. 56
		2. 95
	*/

	function checkDivisibilityBy4(num){
		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);

	// [SECTION] Function as Argument
		// Function parameters can also accept other function as arguments.
		// Some complex functions use other functions as arguments to perform more complicated result.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// function used without a parenthesis is normally associated with using a FUNCTION as an ARGUMENT to another function

	invokeFunction(argumentFunction);

	// this is for finding information about a function using console.log()

	console.log(argumentFunction);

	// [SECTION] Using multiple parameters
		// Multiple "arguments" will correspond to the number of parameters declared in a function in succeeding order

	function createFullName(firstName, middleName, lastName, character){
		console.log(firstName + " " + middleName + " " + lastName + " is the " + character);
	}

	createFullName("Cardo", "J.", "Dalisay", "Probinsyano");

	createFullName("Cardo", "J.", "Dalisay");

	createFullName("Cardo", "J.", "Dalisay", "Probinsyano", "Hello");

	// Use variables as arguments

	let firstName = "Monkey";
	let middleName = "D.";
	let lastName = "Luffy";
	let character = "Protagonist";

	createFullName(firstName, middleName, lastName, character);

	// the order of the argument is the same to the order of the parameters

	function printFullName(middleName, firstName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}

	printFullName("Carlo", "J.", "Caparas");

	/*
		create a function called printFriends
		3 parameters
		My three friends are: friend1, friend2, friend3.

		5mins
	*/

		function printFriends(friend1, friend2, friend3){
		
		console.log("My three friends are: " + friend1 + " , " + friend2 + " , " + friend3);
	}

	printFriends("Ianna", "Joshua", "Myles");

	// [SECTION] The return statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

	function returnFullName(firstName, middleName, lastName) {
		return firstName + " " + middleName + " " + lastName;
		console.log("This will not be printed") ;
	}

	// whatever value is returned from the "returnFullName" fucntion is now stored in the "completeName" variable
	let completeName = returnFullName("Karen", "L.", "Davila");

	console.log(completeName + " is my bestfriend. ");
	console.log(completeName);

	console.log(returnFullName(firstName, middleName, lastName));

	function returnAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress
	}

	let myAddress = returnAddress("Cebu City", "Cebu");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	// when a function has only console.log() to display its result it will return undefined
	let user1 = printPlayerInfo("knight_blank",96,"Paladin");
	console.log(user1);

	// let name = prompt

	function multiplyBy3(num) {
		return product = num * 3;
	}

	let pruduct = multiplyBy3(7);

	console.log(product);

	// OR

	function multiplyNumbers(num1, num2){
		return num1*num2;
	}

	let productOr = multiplyNumbers(3,7);
	console.log("the product of 3 and 7:")
	console.log(productOr)
